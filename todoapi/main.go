package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type TaskTable struct {
	gorm.Model
	Task string
	Done bool
}

// setname add automigate
func (TaskTable) TableName() string {
	return "tasks"

}

type Task struct {
	ID   uint   `json:"id"`
	Task string `json:"task"`
	Done bool   `json:"done"`
}

type NewTask struct {
	Task string
}

//1. axios.post  	 post to  backend  intercept ด้วย gin context c.BindJSON() loggin

func main() {

	db, err := gorm.Open("mysql", "root:password@tcp(127.0.0.1:3306)/taskdb?charset=utf8&parseTime=True")

	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	db.LogMode(true) // dev only!!!!

	if err := db.AutoMigrate(&TaskTable{}).Error; err != nil {
		log.Fatal(err)
	}

	r := gin.Default()

	//alow orgin บอกว่าใครจะมาใช้ของเราได้บ้าง และ ทำ method อะไรได้บ้าง
	// r.Use(cors.New(cors.Config{
	// 	AllowOrigins:     []string{"https://foo.com"},
	// 	AllowMethods:     []string{"PUT", "PATCH"},
	// 	AllowHeaders:     []string{"Origin"},
	// 	ExposeHeaders:    []string{"Content-Length"},
	// 	AllowCredentials: true,
	// 	AllowOriginFunc: func(origin string) bool {
	// 		return origin == "https://github.com"
	// 	},
	// 	MaxAge: 12 * time.Hour,
	// }))
	// r.Run()

	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://localhost:3000"}
	r.Use(cors.New(config))
	// =get task
	r.GET("/tasks", func(c *gin.Context) {
		// tasks := []Task{
		// 	{ID: 1, Task: "do home", Done: true},
		// 	{ID: 2, Task: "read  golang", Done: false},
		// }
		task := []Task{}
		err := db.Find(&task).Error
		if err != nil {
			log.Fatal(err)
		}

		c.JSON(http.StatusOK, task)

	})

	// //add task  แบบทำทับ
	// r.POST("/tasks", func(c *gin.Context) {
	// 	// var task Task
	// 	 task := Task{}
	// 	c.BindJSON(&task)
	// 	fmt.Printf("aaaaaaa %v", task)
	// })
	// r.Run()

	// // add แบบคนละ struc (แนะนำ)

	r.POST("/tasks", func(c *gin.Context) {
		// var task Task
		task := new(NewTask)
		if err := c.BindJSON(&task); err != nil {
			c.JSON(400, gin.H{
				"message": err.Error(),
			})
		}
		taskTable := new(TaskTable)
		taskTable.Task = task.Task
		taskTable.Done = false
		if err := db.Create(taskTable).Error; err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
			c.JSON(201, Task{
				ID:   taskTable.ID,
				Task: taskTable.Task,
				Done: taskTable.Done,
			})
		}

		fmt.Printf("aaaaaaa %v", task)
	})

	type Patch struct {
		Done bool
	}
	// การอัพเดท ข้อมมูล บางตัว
	r.PATCH("/tasks/:id", func(c *gin.Context) {

		task := new(Patch)
		if err := c.BindJSON(task); err != nil {
			c.JSON(400, gin.H{
				"message": err.Error(),
			})
		}
		fmt.Printf("test task %v", task)
		//รับ id มา
		idstr := c.Param("id")

		// กรอง id ก่อนการนำไปใช้
		// if string.TrimSpace(id)==""{
		// c.JSON(404, gin.H{
		// 	"message": err.Error(),
		// })
		// }

		fmt.Printf(" task id %v", idstr)

		// หาว่ามีข้อมูลในก้อนไหม
		taskTable := new(TaskTable)

		// if err == gorm.ErrrecodtNotFound
		// db.Where("id =?",id).First // ต้องใส่อันนี้ด้วย
		if err := db.First(taskTable, idstr).Error; err != nil {
			c.JSON(404, gin.H{
				"message": err.Error(),
			})
		}
		fmt.Printf("find User : %v", taskTable)

		//เอามา กรุป กัน และ ทำการอัพเดท บางตัว
		if err := db.Model(taskTable).Update("done", task.Done).Error; err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
		}
	})

	r.DELETE("/tasks/:id", func(c *gin.Context) {
		idstr := c.Param("id")
		if err := db.Where("id = ?", idstr).Delete(&Task{}).Error; err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
		}
	})

	r.Run()

}
