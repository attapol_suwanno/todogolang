import axios from 'axios';

const host = "http://localhost:8080";


export function listTask() {
    return axios.get(`${host}/tasks`);

}

export function AddTask(task) {
    return axios.post(`${host}/tasks`, task);

}

export function UpdateTask(task) {
    return axios.patch(`${host}/tasks/${task.id}`, task);

}

export function DeleteTask(id) {
    return axios.delete(`${host}/tasks/${id}`);

}