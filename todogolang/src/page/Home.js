
import React, { useState, useEffect } from 'react'
import { AddTask } from '../api/index'
import { listTask } from '../api/index'
import { UpdateTask } from '../api/index'
import { DeleteTask } from '../api/index'
function Home() {
    const [tasks, setTakes] = useState([])
    useEffect(() => {
        listTask()
            .then((response) => setTakes(response.data))
            .catch((err) => console.log(err))
    }, []);



    const [task, setTask] = useState("")


    const handleSubmit = (e) => {
        e.preventDefault();
        if (task.trim() === " ") {
            return;
        }
        const newTask = {
            id: Date.now(),
            task,
            done: false
        }
        // setTakes([...tasks, newTask])
        // AddTask(newTask)

        AddTask({ task })
            .then((resp) => {
                setTakes([...tasks, newTask])
            })
            .catch((err) => console.error(err))
    }

    const handleDelete = (task) => {
        const filter = tasks.filter((t) => t.id !== task.id)
        console.log('delete row :', task.id)

        DeleteTask(task.id)
            .then((resp) => {
                setTakes(filter)
            })
            .catch((err) => console.error(err))
    }

    const handleToggle = (index) => {
        let cloned = [...tasks];

        UpdateTask(cloned[index])
            .then((resp) => {
                cloned[index].done = !cloned[index].done;
            })
            .catch((err) => console.error(err))
        console.log('test click done :', cloned[index])
        setTakes(cloned);

    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input
                    id="task"
                    type="text"
                    placeholder="Input list" onChange={(e) => setTask(e.target.value)}
                />
                <button type="primary"  >Add</button>
            </form>
            <ul>
                {tasks.map((t, index) => (
                    <li key={t.id} onClick={() => handleToggle(index)} >
                        <input type="checkbox" checked={t.done} readOnly></input>

                        {t.done ?
                            <label className="done">{t.task}</label>
                            :
                            <label>{t.task}</label>
                        }
                        <button onClick={(e) => {
                            e.stopPropagation();
                            handleDelete(t)
                        }} >Delete</button>
                    </li>
                ))}
            </ul>
        </div >
    )
}
export default (Home)